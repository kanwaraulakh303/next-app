import Layout from "../components/layout"

const User = ({query}) => {

 const {displayName, ID,displayPic} = query;

  return (
   
    <Layout>
    <div className="row">
    <div className="col-sm-4"></div>
    <div className=".col-sm-8">
    
    <div className="card">
  <div className="card-body">
  <img src={displayPic} />
    <h5 className="card-title">Login success</h5>
    <p className="card-text">Welcome {displayName}</p>
    <p className="raw_profile"></p>
  </div>
  </div>
</div>
    
    
    <div className=".col-sm-4"></div>
  </div>

    </Layout>



  )
}

User.getInitialProps = ({query}) => {
  return {query}
}


export default User;