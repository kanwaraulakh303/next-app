const express = require('express')
const next = require('next')
const passport = require('passport')

//Models entery point 
//require('./models/User')
require('dotenv').config()

const port = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
const auth = require('./auth')

passport.serializeUser(auth.serializeUser)
passport.deserializeUser(auth.deserializeUser)

//passport facebook strategy
passport.use(auth.facebookStrategy)

//passport google strategy
passport.use(auth.googleStrategy)

//Wait for nextjs to spinup
app.prepare().then(() => {

//Nextjs ready create expess  
const server = express()
  require('./routes')(app, server)

  server.get('*', (req, res) => {
    //forward all the requests to Nextjs router
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})