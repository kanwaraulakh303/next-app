const passport = require('passport')
const cookieSession = require('cookie-session')

module.exports = (app, server) => {
  server.use(
    cookieSession({
      maxAge: 30 * 24 * 60 * 60 * 1000,
      keys: [process.env.KEYS_SESSION]
    })
  )

  server.use(passport.initialize())
  server.use(passport.session())

  //map /user route to next js user route
  server.get('/user', (req, res) => {
    console.log("You are inside express route");
    console.log(req.user);
    app.render(req, res, '/user', req.user)
  })

  //trigger facebook auth
  server.get('/auth/facebook', passport.authenticate('facebook'))

  //trigger google auth
  server.get('/auth/google', passport.authenticate('google', { scope: ['profile'] }));

  //facebook callback
  server.get('/auth/facebook/callback', passport.authenticate('facebook', {
    successRedirect: '/user',
    failureRedirect: '/'
  }))

  //google callback
  server.get('/auth/google/callback', passport.authenticate('google', {
    successRedirect: '/user',
    failureRedirect: '/'
  }))
}
