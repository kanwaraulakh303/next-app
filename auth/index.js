const FacebookStrategy = require('passport-facebook').Strategy

const  GoogleStrategy = require('passport-google-oauth20').Strategy;


module.exports = {
  serializeUser: async (user, done) => {
    done(null, user)
  },
  deserializeUser: async (user, done) => {
    done(null, user)
  },
  facebookStrategy: new FacebookStrategy({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_APP_SECRET,
    callbackURL: process.env.FACEBOOK_CALLBACK_URL
  }, async (accessToken, refreshToken, profile, done) => {
    //Facebook auth completed.Hit Orm/db layer to store the sensetive info and auth tokens 
    let user ={ ID: profile.id, displayName: profile.displayName,displayPic:`http://graph.facebook.com/${profile.id}/picture?width=400`,rawProfile:JSON.stringify(profile, null, 4) };
    
    //log user info to console
    console.log("Inside oath app facebook");
    console.log(user);
    done(null, user);
   }),
  googleStrategy: new GoogleStrategy({
    //Google auth completed.Hit Orm/db layer to store the sensetive info and auth tokens 
    clientID: process.env.GOOGLE_CONSUMER_KEY,
    clientSecret: process.env.GOOGLE_CONSUMER_SECRET,
    callbackURL: process.env.GOOGLE_CALLBACK_URL
  },
  function(token, tokenSecret, profile, done) {
    let user ={ ID: profile.id, displayName: profile.displayName,displayPic:profile.photos[0].value,rawProfile:JSON.stringify(profile, null, 4) };
    //log user info to console 
    console.log(user);
    done(null, user);
      })



}
