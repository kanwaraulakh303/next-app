import Navbar from "./navbar"
import Head from "next/head"
const Layout = (props)=>(
<div>
<Head>
   <title>Login Application</title> 
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" />
</Head>    
<Navbar/>
<div className="container">
{props.children}
</div>

</div>
)

export default Layout;