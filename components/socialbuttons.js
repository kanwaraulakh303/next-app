import { useRouter } from 'next/router'

export default ()=>{
  const router = useRouter();
 function handleAuth(type)
  {
if(type==1 || type==2)
{
  if(type==1)
  {
  //type facebook.Redirect the nextjs route to facebook auth handler  
  router.push("/auth/facebook");
  }
  else if(type==2)
  {
  //type Google.Redirect the nextjs route to google auth handler  
  router.push("/auth/google");
  }
}
else
{
  return false;
}
  }
  

 return   (
    <div><button onClick={() => handleAuth("1")} className="btn btn-primary login-buttons"><i className="fa fa-facebook-square"></i> Login With Facebook</button>
             <button onClick={() => handleAuth("2")} className="btn btn-danger login-buttons"><i className="fa fa-google"></i> Login With Google</button></div>
    )
  }

