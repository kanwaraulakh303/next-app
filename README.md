# README #

Nextjs facebook google authentication.

### How do I get set up? ###

* In order to install the application Run **npm init** command at the project root
* Application can be started by runing **npm run dev**  command at the project root. 
* Configure Google Facebook Api credentias in .env file at the project root.
* Current API credentials are for testing purposes only.

